<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Checkinout extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'nik' => $this->nik,
            'checktime' => $this->checktime,
            'checktype' => $this->checktype,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'photo' => $this->photo,
            'link' => [
                'self' => route('projects.show',$this->id)
            ]
        ];
    }
}
