<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Priority;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{
    public $successStatus = 200;

    public function register(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'id' => 'required|unique:users',
                'name' => 'required',
                'email' => 'required|unique:users,email',
                'password' => 'required',
                'c_password' => 'required|same:password',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        
        $user = User::create($input);
        $user->assignRole('karyawan');

        $getUser = User::find($user->id);
        $getUser['api_token'] =  $getUser->createToken('KHADIR API')->accessToken;
        $getUser->save();
        
        $response = [
            "code" => 200,
            "success" => true,
            "data" => new UserResource($getUser),
            "message" => "Success"
        ];
        return response()->json($response, 200);
    }


    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $success['api_token'] =  $user->createToken('KHADIR API')->accessToken;

            $user = User::find($user->id);
            $user->api_token = $user->createToken('KHADIR API')->accessToken;
            $user->save();

            $response = [
                "code" => 200,
                "success" => true,
                "data" => new UserResource($user),
                "message" => "Success"
            ];
            return response()->json($response, 200);
        } else {
            $response = [
                "code" => 401,
                "success" => false,
                "data" => [],
                "message" => "Unauthorised"
            ];
            return response()->json($response, 401);
        }
    }

    public function getUser()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }
}
