<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->id = '00001'; //as nik
        $user->name = 'admin';
        $user->email = 'admin@gmail.com';
        $user->password = Hash::make('1');
        $user->save();

        $token = User::find($user->id);
        $token->api_token = $token->createToken('KHADIR API')->accessToken;
        $token->save();

        $user->assignRole('admin');
    }
}
